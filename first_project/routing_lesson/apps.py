from django.apps import AppConfig


class RoutingLessonConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'routing_lesson'
