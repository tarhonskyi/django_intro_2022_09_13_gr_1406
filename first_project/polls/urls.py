from django.urls import path, include
from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:question_id>/', views.detail, name='detail'),
    path('<int:question_id>/results/', views.results, name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
    path('api/v1/questions/<int:pk>/', views.question_element, name='GET question <id>'),
    path('api/v1/questions/', views.question_collection, name='GET questions'),
    path('accounts/', include('django.contrib.auth.urls'))
]
